---
# 
# Make sure HTML tags are only used in body fields
# Keep the indentation constant
# 
# Look for comments as you go along
#
layout: november_2024
title: Giving Thanks for Open Source
link: https://streamyard.com/watch/kAnCBvtpAeg9
#
# Feature
#
# This is the key message at the beginning of the newsletter
#
feature:
 title: 'WEBINAR—BeagleY-AI: Pi-Compatible and Optimized for Reliability, Openness, AI, and Real-Time'
 body: '<p>Learn how BeagleY-AI lets you deep-dive into the world of embedded Linux, Zephyr, and much more—all backed by an active, diverse community joined by a common passion for collaboration and meaningful real-world projects.</p><p><a href="https://streamyard.com/watch/kAnCBvtpAeg9">Register now</a> for the November 21st webinar</p>.'
#
# Events
#
# Anything on the BeagleBoard.org calendar should go in here
#
#events:
# - title: Embedded World 2020 Exhibition and Conference, Nuremberg, Germany
#   date: February 25-27, 2020 
#   link: https://www.embedded-world.de/en/ausstellerprodukte/embwld20/exhibitor-47280394/beagleboard-org-foundation
# - title: Workshop - Introduction to Embedded Linux and Heterogeneous Computing Using BeagleBone AI at Embedded World 2020, Nuremberg, Germany (Class 3.4)
#   date: Thursday, February 27, 2020 9:30AM-12:30PM
#   link: https://events.weka-fachmedien.de/embedded-world-conference/program/
#
# Cape
#
# The latest featured cape. Must be readily available.
#
#cape:
# title: PocketBeagle® GamePup Cape
# link: https://beagleboard.org/capes
# body: Make your own or emulate handheld games with 160x180 LCD, 10 user programmable buttons, buzzer, LiPo support and more.
# image: https://beagleboard.github.io/newsletter/static/images/Gamepup_80px.jpg
#
# Book
#
# Highlight a recent Beagle book. Images are currently 80px wide.
# 
# TODO: We should probably create an Amazon Smile affiliate link for future books.
#
book:
 title: 'BeagleY-AI: The Latest SBC for AI Applications'
 link: https://www.elektormagazine.com/review/beagley-ai-the-latest-sbc-for-ai-applications
 body: "Elektor writes: &#34;The open-source nature of the BeagleY-AI means that it’s not just a static product ... Users who are willing to engage with the community and contribute to the project will find that they can shape the future of the board...&#34;"
 image: https://www.beagleboard.org/app/uploads/2024/11/20240917104817_20240917-091928.webp
#
# Articles
#
# These are BeagleBoard.org blog posts.
#
# Try to use the short URLs that start with bbb.io/@
#
articles:
 - title: "The IoT Show Podcast: What&#39;s up with BeagleBoard?"
   link: https://creators.spotify.com/pod/show/iotshow/episodes/Whats-up-with-BeagleBoard-e2pqlbk
   body: 'BeagleBoard.org&#39;s Jason Kridner joins Olivier Bloch of The IoT Show where he shares why open-source software needs open-source hardware and how BeagleY-AI makes "easy things easy and hard things possible" for programmers and computer scientists.'
   image: https://www.beagleboard.org/app/uploads/2024/11/39472808-1698366277531-640630fc8313.jpg
 - title: 'This Week in Beagle: BeagleConnect Freedom Adventures'
   link: https://www.beagleboard.org/blog/2024-10-28-this-week-in-beagle-2
   body:  BeagleBoard.org&#39;s Ayush Singh explores adding IEEE 802.15.4 support in MicroPython for BeaglePlay and BeagleConnect Freedom.
 - title: 'This Week in Beagle: The BeagleBoard Rust Imager'
   link: https://www.beagleboard.org/blog/2024-11-04-this-week-in-beagle-3
   body:  This week Ayush is introduced to debcargo, the official way to package Rust applications for Debian, while working on the Rust Imager for flashing BeagleBoard.org hardware. 
#
# Community Topics
#
# Objective is to drive traffic to the discussion list to increase participation.
#
# Be sure to use links that start with beagleboard.org/discuss. You can grab the links from there.
#
topics:
 - title: Cameras supported by BeagleBone® AI TIDL demo
   link: https://groups.google.com/forum/embed/?place=forum/beagleboard&showsearch=true&showpopout=true&showtabs=false&hideforumtitle=true&parenturl=https%3A%2F%2Fbeagleboard.org%2Fdiscuss#!category-topic/beagleboard/newbies/4SBk_JBiKQQ
   body: Ready to add a camera to your BeagleBone® AI? Join this thread for tips from other users.
#
# Projects
#
# Use links that start with bbb.io/+. Just grab the last 6 characters of the project URL to put at the end.
#
projects:
 - title: "GSoC &#39;24: Differentiable Logic for Interactive, Generative Music"
   link: https://ijc8.me/2024/08/26/gsoc-difflogic/
   body: Ian Clester spent the summer integrating differentiable logic, the BeagleBone Black-based Bela sensor processing platform, and bytebeat audio descriptions in a generative music system. 
   image: https://www.beagleboard.org/app/uploads/2024/11/gsoc24-pd-bela.png

---


